package com.twuc.webApp;

public enum BagSize {
    BIG(3),
    MEDIUM(2),
    SMALL(1);

    private final int bagSize;

    BagSize(int bagSize) {
        this.bagSize = bagSize;
    }

    public int size() {
        return bagSize;
    }
}
