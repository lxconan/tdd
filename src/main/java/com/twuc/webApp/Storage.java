package com.twuc.webApp;

import java.util.HashMap;
import java.util.Map;

public class Storage {
    private int capacity;
    private int usedCount;
    private HashMap<SlotSize, Integer> capacityForSlot = new HashMap<>();

    public Storage(int capacity) {
        this(capacity, capacity, capacity);
    }

    public Storage(int small, int medium, int big) {
        this.capacity = small + medium + big;
        capacityForSlot.put(SlotSize.BIG, big);
        capacityForSlot.put(SlotSize.MEDIUM, medium);
        capacityForSlot.put(SlotSize.SMALL, small);
    }

    private static final Map<Ticket, Bag> bags = new HashMap<>();

    public Ticket save(Bag bag, SlotSize slotSize) {
        if (bag != null && !slotSize.valid(bag)) {
            throw new InvalidSizeException();
        }
        Ticket ticket = new Ticket();
        if (usedCount++ < capacityForSlot.get(slotSize)) {
            bags.put(ticket, bag);
            return ticket;
        }
        throw new NoCapacityException("no capacity");
    }

    public Bag retrieve(Ticket ticket) {
        if (!bags.containsKey(ticket)) {
            throw new InvalidTicketException("This an invalid ticket!");
        }
        usedCount--;
        return bags.remove(ticket);
    }
}
