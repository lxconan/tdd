package com.twuc.webApp;

public class Bag {

    private BagSize bagSize;

    public Bag() {
        this(BagSize.BIG);
    }

    public Bag(BagSize bagSize) {
        this.bagSize = bagSize;
    }

    BagSize getSize() {
        return this.bagSize;
    }
}
