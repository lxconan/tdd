package com.twuc.webApp;

public enum SlotSize {
    BIG(3),
    MEDIUM(2),
    SMALL(1);

    private final int slotSize;

    SlotSize(int slotSize) {
        this.slotSize = slotSize;
    }

    public boolean valid(Bag bag) {
        return slotSize >= bag.getSize().size();
    }
}
