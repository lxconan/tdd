package com.twuc.webApp;

public class NoCapacityException extends RuntimeException {
    NoCapacityException(String message) {
        super(message);
    }
}
