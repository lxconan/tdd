package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class StorageTest {
    @Test
    void should_get_ticket_when_save_bag_to_storage() {
        Storage storage = getPlentyCapacityStorage();
        Bag bag = new Bag();

        Ticket ticket = storage.save(bag, SlotSize.BIG);

        assertNotNull(ticket);
    }

    @Test
    void should_get_the_bag_when_retrieve_bag_by_the_ticket() {
        Storage storage = getPlentyCapacityStorage();
        Bag bag = new Bag();
        Ticket ticket = storage.save(bag, SlotSize.BIG);

        Bag retrieveBag = storage.retrieve(ticket);
        assertSame(bag, retrieveBag);
    }

    @Test
    void should_get_ticket_when_save_nothing() {
        Storage storage = getPlentyCapacityStorage();
        Ticket ticket = storage.save(null, SlotSize.BIG);

        assertNotNull(ticket);
    }

    @Test
    void should_get_nothing_when_give_an_invalid_ticket() {
        Storage storage = getPlentyCapacityStorage();
        Ticket invalidTicket = new Ticket();
        assertThrows(InvalidTicketException.class, () -> storage.retrieve(invalidTicket));
    }

    @Test
    void should_get_nothing_when_given_a_used_ticket() {
        Storage storage = getPlentyCapacityStorage();
        Bag bag = new Bag();
        Ticket ticket = storage.save(bag, SlotSize.BIG);
        storage.retrieve(ticket);

        assertThrows(InvalidTicketException.class, () -> storage.retrieve(ticket));
    }

    @Test
    void should_get_nothing_when_retrieve_bag_but_save_nothing() {
        Storage storage = getPlentyCapacityStorage();
        Ticket ticket = storage.save(null, SlotSize.BIG);

        Bag foundBag = storage.retrieve(ticket);

        assertNull(foundBag);
    }

    @Test
    void should_save_bag_when_storage_have_capacity() {
        Storage storage = new Storage(1);
        Ticket ticket = storage.save(new Bag(), SlotSize.BIG);

        assertNotNull(ticket);
    }

    @Test
    void should_not_save_bag_and_get_message_when_storage_not_have_capacity() {
        Storage storage = new Storage(0);

        NoCapacityException exception = assertThrows(NoCapacityException.class,
            () -> storage.save(new Bag(), SlotSize.BIG));

        assertEquals("no capacity", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("provideSavableSlotSizeAndBagSize")
    void should_save_a_bag_to_slot(BagSize bagSize, SlotSize slotSize) {
        Storage storage = getPlentyCapacityStorage();
        Bag bag = new Bag(bagSize);

        Ticket ticket = storage.save(bag, slotSize);

        assertSame(bag, storage.retrieve(ticket));
    }

    @ParameterizedTest
    @MethodSource("provideNotSavableSlotSizeAndBagSize")
    void should_not_save_a_bag_to_slot(BagSize bagSize, SlotSize slotSize) {
        Storage storage = getPlentyCapacityStorage();
        Bag bag = new Bag(bagSize);

        assertThrows(InvalidSizeException.class,
            () -> storage.save(bag, slotSize));
    }

    @Test
    void should_not_save_a_big_bag_when_no_capacity_for_the_big_slot_size() {
        Storage storage = new Storage(0, 0 ,0);

        Bag bag = new Bag(BagSize.BIG);

        assertThrows(NoCapacityException.class, () -> storage.save(bag, SlotSize.BIG));
    }

    @Test
    void should_not_save_a_big_bag_when_big_slot_have_no_capacity() {
        Storage storage = new Storage(0, 0, 1);
        Bag whateverBag = new Bag(BagSize.BIG);
        storage.save(whateverBag, SlotSize.BIG);

        assertThrows(NoCapacityException.class, () -> storage.save(new Bag(BagSize.BIG), SlotSize.BIG));
    }

    @Test
    void should_save_a_big_bag_when_big_slot_is_available() {
        Storage storage = new Storage(0, 0, 1);
        Ticket ticket = storage.save(new Bag(BagSize.BIG), SlotSize.BIG);
        storage.retrieve(ticket);

        assertNotNull(storage.save(new Bag(BagSize.BIG), SlotSize.BIG));
    }

    private static Stream<Arguments> provideSavableSlotSizeAndBagSize() {
        return Stream.of(
            Arguments.of(BagSize.BIG, SlotSize.BIG),
            Arguments.of(BagSize.MEDIUM, SlotSize.BIG),
            Arguments.of(BagSize.MEDIUM, SlotSize.MEDIUM),
            Arguments.of(BagSize.SMALL, SlotSize.BIG),
            Arguments.of(BagSize.SMALL, SlotSize.MEDIUM),
            Arguments.of(BagSize.SMALL, SlotSize.SMALL)
        );
    }

    private static Stream<Arguments> provideNotSavableSlotSizeAndBagSize() {
        return Stream.of(
            Arguments.of(BagSize.BIG, SlotSize.SMALL),
            Arguments.of(BagSize.BIG, SlotSize.MEDIUM),
            Arguments.of(BagSize.MEDIUM, SlotSize.SMALL)
        );
    }

    private Storage getPlentyCapacityStorage() {
        return new Storage(20);
    }
}